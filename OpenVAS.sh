#!/bin/bash

# Настоятельно рекомендую не парится с этой установкой
# Используйте лучше Kali или Parrot OS
#
# Это установка OpenVAS 8 на Debian 8

apt-get update
apt-get upgrade
# Общие требуемые пакеты
apt-get install build-essential cmake bison flex libpcap-dev pkg-config libglib2.0-dev libgpgme11-dev uuid-dev \
sqlfairy xmltoman doxygen libssh-dev libksba-dev libldap2-dev \
libsqlite3-dev libmicrohttpd-dev libxml2-dev libxslt1-dev \
xsltproc clang rsync rpm nsis alien sqlite3 libhiredis-dev libgcrypt11-dev libgnutls28-dev redis-server texlive-latex-base
# Правка конфигурации redis-server
cp /etc/redis/redis.conf /etc/redis/redis.conf.bak
sed  -i -e "s:port 6379:port 0:" /etc/redis/redis.conf
sed  -i -e "s:# unixsocket /tmp/redis.sock:unixsocket /tmp/redis.sock:" /etc/redis/redis.conf
sed  -i -e "s:# unixsocketperm 700:unixsocketperm 777:" /etc/redis/redis.conf
sed  -i -e "s:databases 16:databases 128:" /etc/redis/redis.conf
sed  -i -e "s:# maxclients 10000:maxclients 512:" /etc/redis/redis.conf
/etc/init.d/redis-server restart

# Загрузка сурсов
cd /tmp
wget http://wald.intevation.org/frs/download.php/2291/openvas-libraries-8.0.7.tar.gz
wget http://wald.intevation.org/frs/download.php/2266/openvas-scanner-5.0.5.tar.gz
wget http://wald.intevation.org/frs/download.php/2295/openvas-manager-6.0.8.tar.gz
wget http://wald.intevation.org/frs/download.php/2299/greenbone-security-assistant-6.0.10.tar.gz

tar xvf greenbone-security-assistant-6.0.10.tar.gz
tar xvf openvas-libraries-8.0.7.tar.gz
tar xvf openvas-scanner-5.0.5.tar.gz
tar xvf openvas-manager-6.0.8.tar.gz

# Компиляция и установка

cd openvas-libraries-8.0.7
cmake .
make
make doc
make install
cd ..

cd openvas-manager-6.0.8
cmake .
make
make doc
make install
cd ..

cd openvas-scanner-5.0.5
cmake .
make
make doc
make install
cd ..

cd greenbone-security-assistant-6.0.10
cmake .
make
make doc
make install
cd ..

## Extras

# Установка openvas-smb
# Нафиг не нужная вещь, но если хотите


apt-get install smbclient

apt-get install gcc-mingw-w64 perl-base heimdal-multidev libpopt-dev
cd /tmp
wget http://wald.intevation.org/frs/download.php/1975/openvas-smb-1.0.1.tar.gz
tar xvf openvas-smb-1.0.1.tar.gz

cd openvas-smb-1.0.1
cmake .
make
make doc
make install
ln -s /usr/local/bin/winexe /usr/bin/
cd ..

# Что-то ворнингов многовато при сборке. Но вроде работает. Для тестирования можно использовать.
#
# winexe  -W yourWinDomain -U yourUserName  --interactive=1  //validWinHostIPaddress cmd.exe
#
# Появится commandshell, если вы админ. Для выхода exit
#


# Установка cli. Пригодится
cd /tmp
wget http://wald.intevation.org/frs/download.php/2209/openvas-cli-1.4.3.tar.gz
tar xvf openvas-cli-1.4.3.tar.gz

cd openvas-cli-1.4.3
cmake .
make
make doc
make install
cd ..

apt-get install nmap

# Установка OSP

apt-get install python-setuptools python-paramiko

cd /tmp
wget http://wald.intevation.org/frs/download.php/2177/ospd-1.0.2.tar.gz
wget http://wald.intevation.org/frs/download.php/2005/ospd-ancor-1.0.0.tar.gz
wget http://wald.intevation.org/frs/download.php/2097/ospd-debsecan-1.0.0.tar.gz
wget http://wald.intevation.org/frs/download.php/2003/ospd-ovaldi-1.0.0.tar.gz
wget http://wald.intevation.org/frs/download.php/2149/ospd-paloalto-1.0b1.tar.gz
wget http://wald.intevation.org/frs/download.php/2004/ospd-w3af-1.0.0.tar.gz
wget http://wald.intevation.org/frs/download.php/2181/ospd-acunetix-1.0b1.tar.gz
wget http://wald.intevation.org/frs/download.php/2185/ospd-ikescan-1.0b1.tar.gz
wget http://wald.intevation.org/frs/download.php/2204/ospd-ikeprobe-1.0b1.tar.gz
wget http://wald.intevation.org/frs/download.php/2213/ospd-ssh-keyscan-1.0b1.tar.gz
wget http://wald.intevation.org/frs/download.php/2219/ospd-netstat-1.0b1.tar.gz

find | grep ".tar.gz$" | xargs -i tar zxvfp '{}'

cd ospd-1*
python setup.py install --prefix=/usr/local
cd ../

apt-get install python-pip

cd ospd-ancor-*
sed  -i -e "s:ospd==1.0.0:ospd==1.0.2:" /tmp/ospd-ancor-1.0.0/setup.py
sed  -i -e "s:ospd==1.0.0:ospd==1.0.2:" /tmp/ospd-ancor-1.0.0/ospd_ancor.egg-info/requires.txt
python setup.py install --prefix=/usr/local
cd ../

cd ospd-debsecan-*
python setup.py install --prefix=/usr/local
cd ../

cd ospd-ovaldi-*
sed  -i -e "s:ospd==1.0.0:ospd==1.0.2:" /tmp/ospd-ovaldi-1.0.0/setup.py
sed  -i -e "s:ospd==1.0.0:ospd==1.0.2:" /tmp/ospd-ovaldi-1.0.0/ospd_ovaldi.egg-info/requires.txt
python setup.py install --prefix=/usr/local
cd ../

cd ospd-paloalto-*
sed  -i -e "s:ospd==1.0.1:ospd==1.0.2:" /tmp/ospd-paloalto-1.0b1/setup.py
sed  -i -e "s:ospd==1.0.1:ospd==1.0.2:" /tmp/ospd-paloalto-1.0b1/ospd_paloalto.egg-info/requires.txt
python setup.py install --prefix=/usr/local
cd ../

cd ospd-w3af-*
pip install pexpect
sed  -i -e "s:ospd==1.0.0:ospd==1.0.2:" /tmp/ospd-w3af-1.0.0/setup.py
sed  -i -e "s:ospd==1.0.0:ospd==1.0.2:" /tmp/ospd-w3af-1.0.0/ospd_w3af.egg-info/requires.txt
python setup.py install --prefix=/usr/local
cd ../

cd ospd-acunetix-*
python setup.py install --prefix=/usr/local
cd ../

cd ospd-ikescan-*
python setup.py install --prefix=/usr/local
cd ../

cd ospd-ikeprobe-*
python setup.py install --prefix=/usr/local
cd ../

cd ospd-ssh-keyscan-*
python setup.py install --prefix=/usr/local
cd ../

cd ospd-netstat-*
python setup.py install --prefix=/usr/local
cd ../

# reload libraries
ldconfig

###############################################

#create cert
openvas-mkcert
openvas-mkcert-client -n -i

#create user
openvasmd --create-user=admin --role=Admin && openvasmd --user=admin --new-password=admin

# Генерируем скрипты

echo '#!/bin/bash' > /usr/local/sbin/openvas-update
echo '/usr/local/sbin/openvas-nvt-sync' >> /usr/local/sbin/openvas-update
echo '/usr/local/sbin/openvas-scapdata-sync' >> /usr/local/sbin/openvas-update
echo '/usr/local/sbin/openvas-certdata-sync' >> /usr/local/sbin/openvas-update
chmod +x /usr/local/sbin/openvas-update

echo '#!/bin/bash' > /usr/local/sbin/openvas-start
echo '/usr/local/sbin/openvasmd --rebuild' >> /usr/local/sbin/openvas-start
echo '/usr/local/sbin/openvasmd' >> /usr/local/sbin/openvas-start
echo '/usr/local/sbin/openvassd' >> /usr/local/sbin/openvas-start
echo '/usr/local/sbin/gsad' >> /usr/local/sbin/openvas-start
chmod +x /usr/local/sbin/openvas-start

echo '#!/bin/bash' > /usr/local/sbin/openvas-kill
echo "ps aux | egrep \"(openvas.d|gsad)\" | awk '{print \$2}' | xargs -i kill '{}'" >> /usr/local/sbin/openvas-kill
chmod +x /usr/local/sbin/openvas-kill

rclocal=`cat /etc/rc.local | grep -v "exit 0" | grep -v "openvas"`
echo "$rclocal" > /etc/rc.local
echo "date >> /var/log/openvas_init" >> /etc/rc.local
echo "echo 'openvas init started' >> /var/log/openvas_init" >> /etc/rc.local
echo "/usr/local/sbin/openvas-kill >> /var/log/openvas_init || /bin/true" >> /etc/rc.local
echo "/usr/local/sbin/openvas-start >> /var/log/openvas_init || /bin/true" >> /etc/rc.local
echo "echo 'openvas init finished' >> /var/log/openvas_init" >> /etc/rc.local
echo "exit 0" >> /etc/rc.local


# перезапускаем 

/usr/local/sbin/openvas-kill
/usr/local/sbin/openvas-update
/usr/local/sbin/openvas-start

#
# всё. идем на https://localhost/ user admin password admin
# и в дополнение ко всему утилита для чека
#

cd /usr/local/bin
wget https://svn.wald.intevation.org/svn/openvas/trunk/tools/openvas-check-setup --no-check-certificate
chmod 0755 openvas-check-setup
./openvas-check-setup --v8 --server

apt-get install texlive-full

# ставим dirb

apt-get install libcurl4-gnutls-dev
cd /tmp
wget -c 'http://sourceforge.net/projects/dirb/files/dirb/2.22/dirb222.tar.gz/download' -O dirb222.tar.gz
tar -zxvf dirb222.tar.gz
cd dirb*
chmod +x configure
./configure
make
make install
# Проверка установки:
/usr/local/bin/dirb
ln -s /usr/local/bin/dirb /usr/bin/
cd ..

# Установка nikto

wget https://github.com/sullo/nikto/archive/master.zip
unzip master.zip
cd nikto-master/program
mkdir /usr/local/bin/nikto
cp -fR * /usr/local/bin/nikto/
cp /usr/local/bin/nikto/nikto.conf /etc/nikto.conf
sed  -i -e "s:# EXECDIR=/opt/nikto:EXECDIR=/usr/local/bin/nikto:" /etc/nikto.conf
sed  -i -e "s:# PLUGINDIR=/opt/nikto/plugins:PLUGINDIR=/usr/local/bin/nikto/plugins:" /etc/nikto.conf
sed  -i -e "s:# DBDIR=/opt/nikto/databases:DBDIR=/usr/local/bin/nikto/databases:" /etc/nikto.conf
sed  -i -e "s:# TEMPLATEDIR=/opt/nikto/templates:TEMPLATEDIR=/usr/local/bin/nikto/templates:" /etc/nikto.conf
sed  -i -e "s:# DOCDIR=/opt/nikto/docs:DOCDIR=/usr/local/bin/nikto/docs:" /etc/nikto.conf
chmod +x /usr/local/bin/nikto/nikto.pl
ln -s /usr/local/bin/nikto/nikto.pl /usr/bin/
cd /tmp

# Установка wapiti

apt-get install python-setuptools
wget -O wapiti-2.3.0.tar.gz "http://downloads.sourceforge.net/project/wapiti/wapiti/wapiti-2.3.0/wapiti-2.3.0.tar.gz?r=http://sourceforge.net/projects/wapiti/files/wapiti/wapiti-2.3.0/&ts=1391931386&use_mirror=heanet"
tar zxvf wapiti-2.3.0.tar.gz
cd wapiti-2.3.0
python setup.py install
ln -s /usr/local/bin/wapiti /usr/bin/

# Установка arachni
apt-get install build-essential curl libcurl3 libcurl4-openssl-dev ruby ruby-dev git
git clone git://github.com/Arachni/arachni.git
cd arachni
gem install bundler
bundle install --without prof
rake install

gem install arachni
ln -s /var/lib/gems/2.1.0/gems/arachni-1.4/bin/arachni* /usr/bin









